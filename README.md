## 简介
Zxing-Simplification是对zxing上android项目的简化，包含了三个子项目。

zxing版本：2.3.0

## 子项目
**CaptureActivity**：android项目源码（Barcode Scanner）。增加了注释。   
Barcode Scanner的源码在[《【基于zxing的编解码实战】zxing项目源码解读（2.3.0版本，Android部分）》](http://my.oschina.net/madmatrix/blog/189031)中进行了简单分析

**XBarcodeGenerator**：从Barcode Scanner中分离出来的二维码生成器

**XBarcodeScanner**：从XBarcodeScanner中分离出来的条码扫描器

以上两个子项目的拆解步骤在[《【基于zxing的编解码实战】精简Barcode Scanner篇》](http://my.oschina.net/madmatrix/blog/189036)有简单介绍